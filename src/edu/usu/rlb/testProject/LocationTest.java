package edu.usu.rlb.testProject;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.usu.rlb.project.Location;
import edu.usu.rlb.project.Monkey;
import edu.usu.rlb.project.MonkeyFactory;

public class LocationTest {
	private MonkeyFactory mf;
	
	@Before
	public void setUp(){
		mf = new MonkeyFactory();
	}
	
	@Test
	public void testAddingMonkey() {
		Location loc = new Location("Awesome Place");
		Monkey mon = mf.createMonkey();
		loc.monkeyEnters(mon);
		assertTrue(loc.getAllMonkeys().size() == 1);
	}
	
	@Test
	public void testAddingNeighbor(){
		Location loc1 = new Location("Awesome Place");
		Location loc2 = new Location("Awesomer Place");
		
		loc1.addNeighbor(loc2);
		assertTrue(loc1.getNumNeighbors() == 1);
	}
	
	@Test
	public void testAddingSelfAsNeighbor(){
		Location loc1 = new Location("Awesome Place");
		
		loc1.addNeighbor(loc1);
		assertTrue(loc1.getNumNeighbors() == 1);
	}

}
