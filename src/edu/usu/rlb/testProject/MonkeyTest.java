package edu.usu.rlb.testProject;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import edu.usu.rlb.project.Banana;
import edu.usu.rlb.project.BigOlTexan;
import edu.usu.rlb.project.Bucket;
import edu.usu.rlb.project.Item;
import edu.usu.rlb.project.Monkey;
import edu.usu.rlb.project.MonkeyAggressive;
import edu.usu.rlb.project.MonkeyRandom;
import edu.usu.rlb.project.MonkeyTimid;

public class MonkeyTest {

	@Test
	public void testMonkeyName() {
		
		System.out.println("running test: testMonkeySetup ...");
		
		MonkeyTimid timidMonkey = new MonkeyTimid();
		
		String numMonkeys = "SuperTimmy" + (Monkey.getTotalNumberMonkeys()-1);	// need to subtract one since this number is incremented after it is used
		assertEquals(timidMonkey.getName(),numMonkeys);
	}
	

	@Test
	public void testMonkeyHealth() {
		
		System.out.println("running test: testMonkeyHealth ...");
		
		MonkeyTimid timidMonkey = new MonkeyTimid();
		
		assertTrue(timidMonkey.getHealth() == 20);
	}
	

	@Test
	public void testMonkeyHealthAdjust() {
		
		System.out.println("running test: testMonkeyHealthAdjust ...");
		
		MonkeyTimid timidMonkey = new MonkeyTimid();
		
		BigOlTexan weapon1 = new BigOlTexan();	// BigOlTexan attack deducts 7 points from health
		
		System.out.println(timidMonkey);
		assertTrue(timidMonkey.getHealth() == 20);
		timidMonkey.receiveAttack(weapon1);
		System.out.println(timidMonkey);
		
		assertTrue(timidMonkey.getHealth() == 13);
	}


	@Test
	public void testMonkeyStamina() {
		
		System.out.println("running test: testMonkeyStamina ...");
		
		MonkeyTimid timidMonkey = new MonkeyTimid();
		
		assertTrue(timidMonkey.getStamina() == 20);
	}
	
	
	@Test
	public void testMonkeyStaminaAdjust() {
		
		System.out.println("running test: testMonkeyStaminaAdjust ...");
		
		MonkeyTimid timidMonkey = new MonkeyTimid();
		
		BigOlTexan weapon1 = new BigOlTexan();	// health deducts 7.  stamina drops by 1 when health goes below 15

		assertTrue(timidMonkey.getStamina() == 20);
		timidMonkey.receiveAttack(weapon1);
		assertTrue(timidMonkey.getStamina() == 18);
	}

	
	@Test
	public void testMonkeyStaminaMinimum() {
		
		System.out.println("running test: testMonkeyStaminaMinimum ...");
		
		MonkeyAggressive aggressiveMonkey = new MonkeyAggressive();
		
		aggressiveMonkey.setStamina(11);
		assertTrue(aggressiveMonkey.getStamina() == 11);
		
		aggressiveMonkey.setStamina(10);
		assertTrue(aggressiveMonkey.getStamina() == 10);
		
		aggressiveMonkey.setStamina(9);
		assertTrue(aggressiveMonkey.getStamina() == 10);	// the minimum stamina is 10		
	}
	
	
	@Test
	public void testTotalNumberOfMonkeys() {
		
		System.out.println("running test: testTotalNumberOfMonkeys ...");		
		
		int numberOfMonkeys = Monkey.getTotalNumberMonkeys();
		@SuppressWarnings("unused")
		MonkeyTimid timidMonkey1 = new MonkeyTimid();
		assertTrue(Monkey.getTotalNumberMonkeys() == (numberOfMonkeys + 1));
		
		@SuppressWarnings("unused")
		MonkeyTimid timidMonkey2 = new MonkeyTimid();
		assertTrue(Monkey.getTotalNumberMonkeys() == (numberOfMonkeys + 2));
		
		@SuppressWarnings("unused")
		MonkeyTimid timidMonkey3 = new MonkeyTimid();
		assertTrue(Monkey.getTotalNumberMonkeys() == (numberOfMonkeys + 3));
	}

	
	@Test
	public void testMonkeyIfAlive() {
		
		System.out.println("running test: testMonkeyIfAlive ...");
		
		MonkeyTimid timidMonkey = new MonkeyTimid();
		assertTrue(timidMonkey.getIsAlive() == true);
		
		timidMonkey.setHealth(1);
		assertTrue(timidMonkey.getIsAlive() == true);
				
		timidMonkey.setHealth(0);
		assertTrue(timidMonkey.getIsAlive() == false);
		
		timidMonkey.setHealth(-1);
		assertTrue(timidMonkey.getIsAlive() == false);
	}

	
	@Test
	public void testMonkeyArsenal() {
		
		System.out.println("running test: testMonkeyArsenal ...");
		
		MonkeyRandom randomMonkey = new MonkeyRandom();

		int numWeaponsInArsenal = randomMonkey.getNumOfPooInArsenal();
		assertTrue(numWeaponsInArsenal == 0);		

		BigOlTexan weapon1 = new BigOlTexan();
		randomMonkey.addToArsenal(weapon1);
		assertTrue(randomMonkey.getNumOfPooInArsenal() == numWeaponsInArsenal + 1);		

		randomMonkey.addToArsenal(weapon1);
		assertTrue(randomMonkey.getNumOfPooInArsenal() == numWeaponsInArsenal + 2);
		
		randomMonkey.removeFromArsenal(weapon1);
		assertTrue(randomMonkey.getNumOfPooInArsenal() == numWeaponsInArsenal + 1);

		randomMonkey.removeFromArsenal(weapon1);
		assertTrue(randomMonkey.getNumOfPooInArsenal() == numWeaponsInArsenal + 0);
	}
	

	@Test
	public void testMonkeyItems() {
		
		System.out.println("running test: testMonkeyArsenal ...");
		
		MonkeyRandom randomMonkey = new MonkeyRandom();
		Banana myBanana = new Banana();
		Bucket myBucket = new Bucket();
		List<Item> testItemList = new ArrayList<Item>();

		assertEquals(randomMonkey.getInventory(), testItemList);

		randomMonkey.addItemToInventory(myBanana);
		testItemList.add(myBanana);
		assertEquals(randomMonkey.getInventory(), testItemList);

		randomMonkey.addItemToInventory(myBucket);
		testItemList.add(myBucket);
		assertEquals(randomMonkey.getInventory(), testItemList);

		randomMonkey.addItemToInventory(myBanana);
		testItemList.add(myBanana);
		assertEquals(randomMonkey.getInventory(), testItemList);

		randomMonkey.removeInventoryItem(myBanana);
		testItemList.remove(myBanana);
		assertEquals(randomMonkey.getInventory(), testItemList);
	}
}
