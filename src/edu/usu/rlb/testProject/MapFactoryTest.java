package edu.usu.rlb.testProject;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

import edu.usu.rlb.project.Location;
import edu.usu.rlb.project.MapFactory;
import edu.usu.rlb.project.Monkey;
import edu.usu.rlb.project.MonkeyFactory;

public class MapFactoryTest {
	private List<Monkey> monkeys = new ArrayList<Monkey>();
	private List<Location> locations;
	
	@Before
	public void setUp(){
		MonkeyFactory mf = new MonkeyFactory();
		for(int i=0; i<5; i++){
			monkeys.add(mf.createMonkey());
		}
		locations = MapFactory.generateMap(monkeys);
	}
	
	@Test
	public void testMapSize() {
		assertTrue(locations.size() == 9);
	}
	
	@Test
	public void testAllMonkeysPlaced(){
		int monkeyCount = 0;
		for(Location loc : locations){
			monkeyCount += loc.getAllMonkeys().size();
		}
		assertTrue(monkeyCount == monkeys.size());
	}
	
}
