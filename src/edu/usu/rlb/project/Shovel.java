package edu.usu.rlb.project;

import java.util.List;
import java.util.Scanner;

public class Shovel extends Item{
	private Scanner a = null;
	private String name =  "shovel";
	private int time = 0;
	
	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	public Shovel(){
		a = new Scanner(System.in);
	}
	@Override
	public void getChanges(Monkey luckyFella) {
		List<Monkey> monkeys = luckyFella.getLocation().getMonkeysInRange();
		Monkey badGuy = null;
		System.out.println("Choose the poor sap that is getting a double dose");
		String badMonkey = a.nextLine();
		while(badGuy == null){
			
			for(Monkey m : monkeys) {
				if(m.getName().equalsIgnoreCase(badMonkey)){
					badGuy = m;
					break;
				}
			}
			if(badGuy == null){
				System.out.println("Try again. I couldn't find a monkey with that name\n");
				badMonkey = a.nextLine();
			}
			
		}
		List<Poo> pooOptions = luckyFella.getArsenal();
		Poo poo1 = null;
		System.out.println("Choose Weapon 1:\n");
		String firstPayload = a.nextLine();
		while(poo1 == null){
			for(Poo p: pooOptions){
				if(p.getName().equalsIgnoreCase(firstPayload)) {
					poo1 = p;
					break;
				}
			}
			if(poo1 == null){
				System.out.println("I couldn't find a poo with that name. Gimme another go\n");
				firstPayload = a.nextLine();
			}
		}
		Poo poo2 = null;
		System.out.println("Choose Weapon 2:\n");
		String secondPayload = a.nextLine();
		while(poo2 == null){
			
			for(Poo p: pooOptions){
				if(p.getName().equalsIgnoreCase(secondPayload)) {
					poo2 = p;
					break;
				}
			}
			if(poo2 == null){
				System.out.println("I couldn't find a poo with that name. Gimme another go\n");
				secondPayload = a.nextLine();
			}
			
		}
		
		Command dualFling = new DualFling(poo2, poo1, badGuy, 40);
		luckyFella.addAttack(dualFling);
		return;
	}

	@Override
	public String getName() {
		return name;
	}

}
