package edu.usu.rlb.project;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Driver {
	static List<Monkey> monkeyCharacters = null; 
	static Monkey playerMonkey = null;
	public static void main(String[] args) {

		monkeyCharacters = new ArrayList<Monkey>();
		MonkeyFactory monkeyFactory = new MonkeyFactory();

		Scanner a = new Scanner(System.in);
		System.out.println("You there! What's your name, chimp?");
		String name = a.nextLine();
		playerMonkey = new HumanPlayer(25, name);
//		playerMonkey = new HumanPlayer(1, name); // to test dying
		System.out.println("You've escaped the Monkey Cage. How many escaped with you? (enter a number between 1 and 6)");
		int numMonkeys = a.nextInt();
		while(numMonkeys < 1 || numMonkeys > 6) {
			if(numMonkeys < 1){
				System.out.println("Someone's gotta escape, or we have no game.");
				numMonkeys = a.nextInt();
			}
			else {
				System.out.println("There weren't that many monkeys in the cage to begin with.");
				numMonkeys = a.nextInt();
			}
		}
		System.out.println("You have entered the Arena with " + numMonkeys + " other monkeys. There can be only one!");
		monkeyCharacters.add(playerMonkey);
		while(monkeyCharacters.size() < numMonkeys + 1){
			monkeyCharacters.add(monkeyFactory.createMonkey());
		}
		// make the map
		MapFactory.generateMap(monkeyCharacters);

		//initiate the gameLoop 
		GameLoop gameloop = new GameLoop(monkeyCharacters);
		gameloop.start();
		a.close();
	}
	
}
