package edu.usu.rlb.project;

import java.util.List;
import java.util.Random;

public class MonkeyRandom extends Monkey {

	public MonkeyRandom() {
		super("Randy");
		Random rand = new Random(); 
		int randomNum = rand.nextInt(10);
		this.health = randomNum + 15;	// this gives a health of 15-25

		randomNum = rand.nextInt(10);
		this.stamina = randomNum + 15;	// this gives a stamina of 15-25
	}

	public void doTurn(){
		// first we get all monkey's in range
		List<Monkey> monkeys = location.getMonkeysInRange();
		// we need to remove ourselves from this list
		monkeys.remove(monkeys.indexOf(this));

		if(monkeys.size() == 0){
			// move
			List<Location> locations = location.getNeighboringLocations();
			int rand = new Random().nextInt(locations.size());
			Location next = locations.get(rand);
			next.monkeyEnters(this);
			location.monkeyLeaves(this);
			location = next;
		}else if(invoker.attackList.size() == 0){		
			// now we find a random monkey to attack
			Random rand = new Random(); 

			int randomNum = rand.nextInt(monkeys.size());
			Monkey randomMonkey = monkeys.get(randomNum);

			randomNum = rand.nextInt(getNumOfPooInArsenal());
			Poo randomPoo = getArsenal().get(randomNum);
			removeFromArsenal(randomPoo);

			addAttack(new FlingPoo(randomPoo, randomMonkey, 10));
			doAttack();
		}else{
			doAttack();
		}

		while(getArsenal().size() < getArsenalMaxSize()){
			addToArsenal(PooFactory.makeAPoo());
		}
	}
}
