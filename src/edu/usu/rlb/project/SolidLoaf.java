package edu.usu.rlb.project;

public class SolidLoaf extends Poo{
	private int healthDeduction = 4;
	private int staminaDeduction = 1;
	private String name = "SolidLoaf";
	
	public SolidLoaf(){}
	
	public void getChanges(Monkey m){
		m.setHealth(m.getHealth() - healthDeduction);
		if(m.getHealth() <= 15){
			m.setStamina(m.getStamina()-1); // TODO probably should subtract staminaDeduction here instead of 1
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void undoChanges(Monkey m) {
		if(m.getHealth() <= 15){
			m.setStamina(m.getStamina() + staminaDeduction);	
		}
		m.setHealth(m.getHealth() + healthDeduction);
	}
	
	@Override
	public int getPooDamage() {
		return healthDeduction;
	}

	@Override
	public int getPooStaminaModifier() {
		return staminaDeduction;
	}
	
}
