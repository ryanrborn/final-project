package edu.usu.rlb.project;

public class TurkeySquirts extends Poo{
	private int healthDeduction = 5;
	private int staminaDeduction = 2;
	private String name = "TurkeySquirts";
	
	public TurkeySquirts(){}
	
	public void getChanges(Monkey m){
		m.setHealth(m.getHealth() - healthDeduction);
		m.setStamina(m.getStamina() - staminaDeduction);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void undoChanges(Monkey m) {
		m.setHealth(m.getHealth() + healthDeduction);
		m.setStamina(m.getStamina() + staminaDeduction);
	}
	
	@Override
	public int getPooDamage() {
		return healthDeduction;
	}

	@Override
	public int getPooStaminaModifier() {
		return staminaDeduction;
	}
}
