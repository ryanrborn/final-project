package edu.usu.rlb.project;

public abstract class Item {
	int time;
	public abstract void getChanges(Monkey luckyFella);
	public abstract String getName();
}
