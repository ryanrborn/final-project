package edu.usu.rlb.project;

public class JustAFart extends Poo{
	private int healthDeduction = 0;
	private int staminaDeduction = 0;
	private String name = "JustAFart";
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public JustAFart() {}

	@Override
	public void getChanges(Monkey m) {
		m.setHealth(m.getHealth() - healthDeduction);
		m.setStamina(m.getStamina() - staminaDeduction);
	}

	@Override
	public void undoChanges(Monkey m) {
		m.setHealth(m.getHealth() + healthDeduction);
		m.setStamina(m.getStamina() + staminaDeduction);
	}
	
	@Override
	public int getPooDamage() {
		return healthDeduction;
	}

	@Override
	public int getPooStaminaModifier() {
		return staminaDeduction;
	}
}
