package edu.usu.rlb.project;

public class DualFling implements Command {
	
	private FlingPoo implementOfDestruction = null;
	private FlingPoo theSecondOneYouDidntSeeComing;
	private Monkey poorSap;
	private int time;
	public DualFling(Poo yuck, Poo gross, Monkey aboutToHateLife, int time) {
		this.implementOfDestruction = new FlingPoo(yuck, aboutToHateLife, 0);
		this.theSecondOneYouDidntSeeComing = new FlingPoo(gross, aboutToHateLife, 0);
		this.poorSap = aboutToHateLife;
		this.time = time;
	}
 
	public int getTime(){
		return time;
	}
	
	public void setTime(int time) {
		this.time = time;
	}
	//the time is how long it takes to execute a command
	public void reduceTime(int monkeyStamina) {
		time -= monkeyStamina;
	}
	@Override
	public void execute() {
		implementOfDestruction.execute();
		theSecondOneYouDidntSeeComing.execute();
	}

	@Override
	public void unexecute() {
		implementOfDestruction.unexecute();
		theSecondOneYouDidntSeeComing.unexecute();
	}
	
	public String getPooName(){
		return implementOfDestruction.getPooName() + " and " + theSecondOneYouDidntSeeComing.getPooName();
	}

	public String getRecieverName() {
		return poorSap.getName();
	}
}
