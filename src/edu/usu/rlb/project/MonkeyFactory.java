package edu.usu.rlb.project;

import java.util.Random;

public class MonkeyFactory {
	public Monkey createMonkey(){
		Monkey product = null;
		Random rand = new Random(); 
		int randomNum = rand.nextInt(100);

		if(randomNum < 25)
		{
			product = new MonkeyAggressive();
		}
		
		else if (randomNum < 50) 
		{
			product = new MonkeyTimid();
		}
		
		else if (randomNum < 75) 
		{
			product = new MonkeySurvivor();
		}
		
		else
		{
			product = new MonkeyRandom();
		}

		while(product.getArsenal().size() < product.getArsenalMaxSize()){
			product.addToArsenal(PooFactory.makeAPoo());
		}

		return product;
	}
}
