package edu.usu.rlb.project;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GameLoop {
	private List<Monkey> monkeys;
	
	public GameLoop(List<Monkey> monkeys){
		this.monkeys = monkeys;
	}
	
	public void start(){
		boolean gameEnd = false;
		while(!gameEnd){
			for(Monkey monkey : monkeys){
				if(monkey.isAlive){
					monkey.doTurn();
				}
				int result = checkForEndGame();
				if(result == 0){
					// player's dead
					System.out.println("You stagger to your little monkey knees,\n"
					+"as you lift your hand from your side you see that it is\n"
					+"covered in disgusting poo... You're growing weak, you try\n"
					+"to stand back up, but the weight of the poo is too much.\n"
					+"All you can do is just lie there, and try not to asphyxiate\n"
					+"from the toxic fumes.\n\nThe End.");
					gameEnd = true;
					printPooPile();
					break;
				}else if(result == 1){
					// player won
					System.out.println("You look around you and find that there is no\n"
					+"one left. You search the zoo only to find hardly distinguishable mounds\n"
					+"of poo littering the ground. You make your way to the zoo's entrance,\n"
					+"push the door open and walk out into the city.\n\nThe End.");
					gameEnd = true;
					printPooPile();
					break;
				}
				try {
					if(monkey.isAlive){
						Thread.sleep(1000);
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			boolean monkeyDied = false;
			List<Integer> toRemove = new ArrayList<Integer>();
			if(!gameEnd){
				System.out.println("\nThese are the monkeys that are still alive:");
				for(Monkey monkey : monkeys){
					if(monkey.isAlive){
						System.out.println(monkey);
					}
					else
					{
						// queue monkeys to be removed (to avoid ConcurrentModificationException)
						toRemove.add(monkeys.indexOf(monkey));
						monkeyDied = true;
					}
				}
			}
			if (monkeyDied == true)
			{
				// now we can remove the dead monkeys
				// we go backwards over the loop to avoid index
				// out of bounds when removing more than one monkey
				Collections.sort(toRemove, Collections.reverseOrder()); 
				for(Integer i : toRemove){
					monkeys.remove(i.intValue());
				}
				// awarding live monkeys
				for (Monkey monkey : monkeys)
				{
					monkey.setHealth(monkey.getHealth() + 2);
					monkey.setStamina(monkey.getStamina() + 2);
				}
			}
		}
	}
	
	private void printPooPile() {
		System.out.println("\n"+
"			     (   )\n"+
"			     (   ) (\n"+
"    		      ) _   )\n"+
"			       ( \\_\n"+
"			     _(_\\ \\)__\n"+
"			    (____\\___)))\n");
	}

	private int checkForEndGame(){
		if(!monkeys.get(0).isAlive){
			return 0;
		}
		boolean allDead = true;
		for(int i = 1; i<monkeys.size(); i++){
			if(monkeys.get(i).isAlive){
				allDead = false;
				break;
			}
		}
		if(allDead){
			return 1;
		}
		return -1;
	}
}
