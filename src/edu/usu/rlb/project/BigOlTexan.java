package edu.usu.rlb.project;

public class BigOlTexan extends Poo{
	private int healthDeduction = 7;
	private int staminaDeduction = 2;
	private String name = "BigOlTexan";
	
	public BigOlTexan(){}
	
	public void getChanges(Monkey m){
		m.setHealth(m.getHealth() - healthDeduction);
		m.setStamina(m.getStamina()-staminaDeduction);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void undoChanges(Monkey m) {
		m.setHealth(m.getHealth() + healthDeduction);
		m.setStamina(m.getStamina() + staminaDeduction);
		
	}

	@Override
	public int getPooDamage() {
		return healthDeduction;
	}

	@Override
	public int getPooStaminaModifier() {
		return staminaDeduction;
	}
}
