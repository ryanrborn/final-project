package edu.usu.rlb.project;

//this is the new implementation of commands. it makes it so poo is a strategy, and various types of poo can all be added to the arsenal
public class FlingPoo implements Command{
	private Poo implementOfDestruction;
	private Monkey poorSap;
	private int time;
	public FlingPoo(Poo yuck, Monkey aboutToHateLife, int time) {
		this.implementOfDestruction = yuck;
		this.poorSap = aboutToHateLife;
		this.time = time;
	}
 
	public int getTime(){
		return time;
	}
	
	public void setTime(int time) {
		this.time = time;
	}
	//the time is how long it takes to execute a command
	public void reduceTime(int monkeyStamina) {
		time -= monkeyStamina;
	}
	@Override
	public void execute() {
		implementOfDestruction.getChanges(poorSap);
	}

	@Override
	public void unexecute() {
		implementOfDestruction.undoChanges(poorSap);
	}
	public String getPooName(){
		return implementOfDestruction.getName();
	}

	public String getRecieverName() {
		return poorSap.getName();
	}
}
