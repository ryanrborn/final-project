package edu.usu.rlb.project;

public interface Command {
	public void execute();
	public void unexecute();
	public void reduceTime(int stamina);
	public int getTime();
	public String getPooName();
	public String getRecieverName();
}
