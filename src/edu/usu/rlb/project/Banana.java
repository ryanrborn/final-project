package edu.usu.rlb.project;

public class Banana extends Item{
	private String name = "banana";
	private int time = 10;
	
	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	@Override
	public void getChanges(Monkey luckyFella) {
		luckyFella.setStamina(luckyFella.getStamina() + 10);
		System.out.println("Eating this banana has restored 10 Stamina");
		luckyFella.removeInventoryItem(this);
	}

	@Override
	public String getName() {
		return name;
	}
	
}
