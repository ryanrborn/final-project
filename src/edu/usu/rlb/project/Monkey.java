package edu.usu.rlb.project;

import java.util.ArrayList;
import java.util.List;

public abstract class Monkey {
	protected String name;
	protected static int nameNumber = 0;
	protected int health;
	protected int stamina;
	protected List<Item> inventory = new ArrayList<Item>();
	protected List<Poo> arsenal = new ArrayList<Poo>();
	protected Location location;
	protected int arsenalSize = 5;
	protected boolean isAlive = true;
	protected MonkeyInvoker invoker = new MonkeyInvoker();
	
	public Monkey()
	{
	}
	
	public Monkey(String name)
	{
		this.name = name+Monkey.nameNumber;
		Monkey.nameNumber++;
	}
	
	public abstract void doTurn();
	
	public void receiveAttack(Poo poo)
	{
		poo.getChanges(this);
	}
	
	public void unReceiveAttack(Poo poo)
	{
		poo.undoChanges(this);	
	}
	
	public void addAttack(Command c)
	{
		invoker.addAttack(c);
	}
	
	public void doAttack()
	{
		
		if (invoker.attackList.isEmpty())
		{
			return;
		}
		Command c =  invoker.getDoAttack();
		c.reduceTime(this.stamina);
		if(c.getTime() <= 0){
			System.out.println(name+" flings "+c.getPooName() + " at " + c.getRecieverName());
			c.execute();
			invoker.removeAttack();
		}
	}
	
	public void undoAttack()
	{
		Command c = invoker.getUndoAttack();
		if (c != null)
		{
			System.out.println("Undoing attack:   (" + c + ")");
			isAlive = true;	// I think this will always bring them back to life?
			c.unexecute();
		}
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static int getTotalNumberMonkeys() {
		return nameNumber;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;

		if (this.health <= 0)
		{
			this.health = 0;
			isAlive = false;
			System.out.println(name+" has fallen!");
		}
	}
	
	public void setArsenalMaxSize(int size) {
		this.arsenalSize = size;
	}
	
	public List<Poo> getArsenal() {
		return arsenal;
	}
	
	public void removeFromArsenal(Poo p){
		arsenal.remove(arsenal.indexOf(p));
	}
	
	public void addToArsenal(Poo p){
		arsenal.add(p);
	}
	
	public int getArsenalMaxSize(){
		return arsenalSize;
	}
	
	public int getNumOfPooInArsenal(){
		return arsenal.size();
	}
	
	public int getStamina() {
		return stamina;
	}

	public void setStamina(int stamina) {
		this.stamina = stamina;

		if (this.stamina <= 10)
		{
			this.stamina = 10;
		}
	}

	public boolean getIsAlive() {
		return isAlive;
	}

	public void setAlive(boolean isAlive) {
		this.isAlive = isAlive;
	}

	public MonkeyInvoker getInvoker()
	{
		return invoker;
	}

	public MonkeyInvoker getMonkeyListStatus()
	{
		return invoker;
	}
	
	@Override
	public String toString()
	{
		return String.format("%-14shealth: %-6dstamina: %-5d", name, health, stamina);
	}

	public List<Item> getInventory() {
		return inventory;
	}
	
	public Location getLocation() {
		return location;
	}
	
	public void addItemToInventory(Item i) {
		inventory.add(i);
	}
	public void removeInventoryItem(Item i) {
		inventory.remove(inventory.indexOf(i));
	}
}
