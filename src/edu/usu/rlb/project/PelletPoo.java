package edu.usu.rlb.project;

public class PelletPoo extends Poo{
	private int healthDeduction = 1;
	private int staminaDeduction = 0;
	private String name = "PelletPoo";
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public PelletPoo() {}

	@Override
	public void getChanges(Monkey m) {
		m.setHealth(m.getHealth() - healthDeduction);
		m.setStamina(m.getStamina() - staminaDeduction);
	}

	@Override
	public void undoChanges(Monkey m) {
		m.setHealth(m.getHealth() + healthDeduction);
		m.setStamina(m.getStamina() + staminaDeduction);
	}
	
	@Override
	public int getPooDamage() {
		return healthDeduction;
	}

	@Override
	public int getPooStaminaModifier() {
		return staminaDeduction;
	}
}
