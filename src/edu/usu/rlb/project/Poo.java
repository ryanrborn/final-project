package edu.usu.rlb.project;


public abstract class Poo{
	public abstract void getChanges(Monkey m);
	public abstract void undoChanges(Monkey m);
	public abstract String getName();
	public abstract int getPooDamage();
	public abstract int getPooStaminaModifier();
}
