package edu.usu.rlb.project;

import java.util.concurrent.ConcurrentLinkedDeque;

public class MonkeyInvoker {
	protected ConcurrentLinkedDeque<Command> attackList = new ConcurrentLinkedDeque<Command>();
	protected ConcurrentLinkedDeque<Command> undoList = new ConcurrentLinkedDeque<Command>();

	public void addAttack(Command c)
	{
		attackList.add(c);
		
		if (!undoList.isEmpty())
		{
			undoList.clear();
		}
	}
	
	public Command getDoAttack()
	{
		return attackList.peek();
	}
	
	public void removeAttack(){
		undoList.push(attackList.pop());
	}
	
	public int getAttackListSize(){
		return attackList.size();
	}
	
	public Command getUndoAttack()
	{
		Command c = undoList.peek();
		
		if (c != null)
		{
			undoList.pop();
			attackList.push(c);
		}
		
		return c;
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append("Commands to do:\n");
		Command[] list = attackList.toArray(new Command[0]);
		for (int i = 0; i < list.length; i++)
		{
			sb.append("\t" + list[i].toString());
			sb.append("\n");
		}
		
		sb.append("Commands to undo:\n");
		Command[] ulist = undoList.toArray(new Command[0]);
		for (int i = 0; i < ulist.length; i++)
		{
			sb.append("\t" + ulist[i].toString());
			sb.append("\n");
		}
		
		return sb.toString();
	}
}
