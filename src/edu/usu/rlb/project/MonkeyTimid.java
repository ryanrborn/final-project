package edu.usu.rlb.project;

import java.util.ArrayList;
import java.util.List;

public class MonkeyTimid extends Monkey{

	public MonkeyTimid() {
		super("SuperTimmy");
		this.health = 20;
		this.stamina = 20;
	}

	public void doTurn(){
		// first we get all monkey's in range
		List<Monkey> monkeys = location.getMonkeysInRange();
		// we need to remove ourselves from this list
		monkeys.remove(monkeys.indexOf(this));

		if(monkeys.size() == 0){
			// if no monkeys are in range, stay here
			return;
		}else{
			// if there is an empty location, move to it

			// load list of neighboring locations
			List<Location> testLocations = new ArrayList<Location>();
			testLocations = location.getNeighboringLocations();

			// Check each location to see if there are monkeys there
			List<Monkey> testMonkeyList = new ArrayList<Monkey>();
			Location newTimidLoc = null;
			boolean timidMove = false;
			for (Location loc : testLocations)
			{
				testMonkeyList = loc.getLiveMonkeys();
				// if a location has 0 monkeys, we want to move there
				if (testMonkeyList.size() == 0)
				{
					newTimidLoc = loc;
					timidMove = true;
					break;
				}
			}

			// if an empty location is found, move to it
			if (timidMove == true)
			{
				location.monkeyLeaves(this);
				this.location = newTimidLoc;
				location.monkeyEnters(this);
			}
			// if an empty location is not found, attack the strongest monkey
			else if(getInvoker().getAttackListSize() == 0)
			{
				// first we get all monkey's in range
				monkeys = location.getMonkeysInRange();
				// we need to remove ourselves from this list
				monkeys.remove(monkeys.indexOf(this));

				// now we find the strongest monkey to attack
				Monkey strongest = null;
				for(Monkey mon : monkeys){
					if(strongest == null){
						strongest = mon;
					}else if(strongest.getHealth() < mon.getHealth()){
						strongest = mon;
					}
				}

				Poo biggest = null;
				for(Poo p : getArsenal()){
					if(biggest == null){
						biggest = p;
					}else if(biggest.getPooDamage() < p.getPooDamage()){
						biggest = p;
					}
				}
				removeFromArsenal(biggest);

				addAttack(new FlingPoo(biggest, strongest, 10));		
				doAttack();
			}else{
				doAttack();
			}
			while(getArsenal().size() < getArsenalMaxSize()){
				addToArsenal(PooFactory.makeAPoo());
			}
		}
	}
}
