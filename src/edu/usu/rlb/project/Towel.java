package edu.usu.rlb.project;

public class Towel extends Item{
	private String name = "towel";

	@Override
	public void getChanges(Monkey luckyFella) {
		Command c = luckyFella.getInvoker().getUndoAttack();
		if(c != null){
			c.unexecute();
			System.out.println("Using this towel has undone the last attack you received!");
			luckyFella.removeInventoryItem(this);
		}
		else{
			System.out.println("Can't undo anything");
		}
		
	}

	@Override
	public String getName() {
		return name;
	}

}
