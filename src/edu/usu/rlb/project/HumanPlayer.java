package edu.usu.rlb.project;

import java.util.List;
import java.util.Scanner;

public class HumanPlayer extends Monkey{
//	private String name = null;
	private static boolean endTurn = false;
//	private Location myLocation = null;
	public HumanPlayer(int initialHealth, String name) {
		this.health = initialHealth;
		this.name = name;
		while(getArsenal().size() < getArsenalMaxSize()){
			addToArsenal(PooFactory.makeAPoo());
		}
		this.stamina = 25;
		printMonkey();
	}
	public void printMonkey(){
	    System.out.println("                                 ___\n"+
	"                                / _,\\ \n"+
	"                                \\_\\ \n" +
	"                        ,,,,    _,_)  #      /)\n"+
	"                       (= =)D__/    __/     //\n"+
	"                      C/^__)/     _(    ___//\n"+
	"                        \\_,/  -.   '-._/,--'\n"+
	"                  _\\\\_,  /           -//.\n"+
	"                   \\_ \\_/  -,._ _     ) )\n"+
	"                      \\/    /    )    / /\n"+
	"                      \\-__,/    (    ( (\n"+
	"                                 \\.__,-)\\_\n"+
	"                                 )\\_ / -(\n"+
	"                                / -(////\n"+
	"                               ////\n"
	);
	}
	public void doTurn(){
		endTurn = false;
		
		Scanner a = new Scanner(System.in); // we ought to create a singleton to get rid of the warning here
		
		printPrompt();
		String input = a.nextLine();
		
		while(input.equals("quit") == false) {
			if(!endTurn){
				doActionBasedOnStringInput(input);
				if(!endTurn){
					input = a.nextLine();
				}
			}
			else{	// was causing an infinite loop when 'endTurn = true'
				break;
			}
		}
		doAttack();

		//a.close();
	}
	private void printPrompt() {
		printMonkeysInRange(this.location);
		System.out.println("Enter command. Type \"help\" for list of commands.");
	}
	private void doActionBasedOnStringInput(String s) {
		String[] commandTokens = s.split(" ");		
		if (commandTokens[0].equalsIgnoreCase("attack")) {
			Monkey reciever = null;
			Poo ammo = null;
			if(commandTokens.length<2){
				printError();
				return;
			}
			if(commandTokens[1] != null) {
				if(!commandTokens[1].equalsIgnoreCase(this.getName()))
				{
					for(Monkey m : location.getMonkeysInRange()){
						if(m.getName().equalsIgnoreCase(commandTokens[1])){
							reciever = m;
							break;
						}
					}
				}
				if(reciever == null){
					printError();
					return;
				}
			}
			if(commandTokens.length<3){
				printError();
				return;
			}
			if(commandTokens[2] != null){
				for(Poo p : this.getArsenal()){
			    		if(commandTokens[2].equalsIgnoreCase(p.getName())) {
			    			ammo = p;
			    			break;
			    		}
			    	}
			    	if(ammo == null){
			    		printError();
			    		return;
			    	}
			    }
			    Command c = new FlingPoo(ammo, reciever, 10);
				removeFromArsenal(ammo);
			    this.addAttack(c);

				while(getArsenal().size() < getArsenalMaxSize()){
					addToArsenal(PooFactory.makeAPoo());
				}
			    
			    endTurn = true;
		   }
		   
			else if (commandTokens[0].equalsIgnoreCase("help")){
			   printCommandList();
		   }
		   
			else if(commandTokens[0].equalsIgnoreCase("moveto")){
			   List<Location> adjacent = this.location.getNeighboringLocations();
			   if(commandTokens.length<2){
					printError();
					return;
				}
			   if(commandTokens[1] != null) {
				   for(Location l : adjacent) {
					   if(l.getName().equalsIgnoreCase(commandTokens[1])) {
						   this.location.monkeyLeaves(this);
						   l.monkeyEnters(this);
						   this.location = l;
						   printItems(l);
						   printMonkeysInRange(l);
						   endTurn = true;
						   return;
						   // break; // could break here since it keeps looping after location is found, but shouldn't hurt to have in.
					   }
				   }
				   printError();
			   }
			   else{
				   printError();
				   return;
			   }
			  
		   }
		   
			else if(commandTokens[0].equalsIgnoreCase("move")){
				if(commandTokens.length<2){
					printError();
					return;
				}
			   if(commandTokens[1].equalsIgnoreCase("options")){
				   List<Location> adjacent = this.location.getNeighboringLocations();
				   for(Location l : adjacent) {
					   System.out.println(l.getName());
				   }
			   }
			   else {
				   printError();
				   return;
			   }
		   }
		   
			else if(commandTokens[0].equalsIgnoreCase("use")) {
				if(commandTokens.length<2){
					printError();
					return;
				}
			   if(commandTokens[1] != null){
				   List<Item> inventory = this.getInventory();
				   Item useable = null;
				   for(Item i : inventory){
					   if(i.getName().equalsIgnoreCase(commandTokens[1])){
						   useable = i;
					   }
				   }
				   if(useable == null){
					   printError();
				   }
				   else{
					   useable.getChanges(this);
					   endTurn = true;
				   }
			   }
		   }
		   
			else if(commandTokens[0].equalsIgnoreCase("arsenal")) {
			   printArsenal(this.getArsenal());			   
		   }
		   
			else if(commandTokens[0].equalsIgnoreCase("inventory")){
			   printInventory(this.getInventory());
		   }
			
			else if(commandTokens[0].equalsIgnoreCase("details")){
			   printActionDetails();
		   }
		
			else if(commandTokens[0].equalsIgnoreCase("map")){
				printMap();
		   }
		
			else if(commandTokens[0].equalsIgnoreCase("state")){
			   printState();
		   }
			else if(commandTokens[0].equalsIgnoreCase("pickup")){
			   if(commandTokens[1] != null){
				   if(this.location.hasItem(commandTokens[1])){
					   Item i = null;
					try {
						i = location.getItem(commandTokens[1]);
					} catch (Exception e) {
						e.printStackTrace();
					}
					   this.addItemToInventory(i);
					   System.out.println("You have picked up a " + i.getName());
					   endTurn = true;
				   }
			   }   
		   }
			else if(commandTokens[0].equalsIgnoreCase("")){
				System.out.println(">");	// use this to try to catch false scanner reads
			}
			else{
				printError();
				return;
			}
		   
	}
	
	private void printMap(){
		System.out.println(
		"*--------------------#--------------------#--------------------*\n"+
		"|                    |                    |                    |\n"+
		"|     Monkey Tree    |     Hippo Pool     |     Bear Cave      |\n"+
		"|                    |                    |                    |\n"+
		"#--------------------#--------------------#--------------------#\n"+
		"|                    |                    |                    |\n"+
		"|    Zebra Corral    |     Courtyard      | Elephant Enclosure |\n"+
		"|                    |                    |                    |\n"+
		"#--------------------#--------------------#--------------------#\n"+
		"|                    |                    |                    |\n"+
		"|  Zookeepers Office |     Tiger Den      |       Aviary       |\n"+
		"|                    |                    |                    |\n"+
		"#--------------------#--------------------#--------------------#\n"
		);
	}
	
	private void printMonkeysInRange(Location l) {
		System.out.println("\nThe following Monkeys are in close-quarter combat:");
		List<Monkey> monkeysInRange = this.location.getMonkeysInRange();
		monkeysInRange.remove(monkeysInRange.indexOf(this));
		if(monkeysInRange.size() >0){
			for(Monkey m : monkeysInRange){
				if(m.isAlive){
					System.out.println(m);
				}
			}

		}else{
			System.out.println("none");
		}
		System.out.println();
	}
	private void printItems(Location l) {
		System.out.println("You are now in the " + l.getName());
		if(l.getItems().size() > 0) {
			System.out.println("\nYou see that there is lying about:");
			List<String> items = l.getItems();
			for(String i : items){
				System.out.println("a " + i);
			}
		}else{
			System.out.println("There isn't anything lying about.");
		}
		
	}
	private void printState() {
		System.out.println("You are standing in " + this.getLocation() +"\nYour health is " + this.getHealth() + "\nYour stamina is " + this.getStamina());
		printMonkeysInRange(location);
	}
	
	
	private void printActionDetails() {
		System.out.println("Action Details: \n" +
				"Attack: Takes 60 Stamina to execute. Flings a Poo in your arsenal at an opposing monkey.\n" +
				"Moveto: Takes 90 Stamina to execute. Moves you to the target Location.\n" + 
				"Use:    Takes a variable amount of Stamina. Executes the item's effect.\n");
	}
	private static void printInventory(List<Item> inventory) {
		System.out.println("You are carrying:\n");
		for(Item i : inventory) {
			System.out.println(i.getName());
		}
		if(inventory.size() == 0){
			System.out.println("Nothing");
		}
	}
	private static void printArsenal(List<Poo> arsenal) {
		for(Poo p: arsenal) {
			String arsenalString = String.format("%-22sDamage: %-5dStamina Modifier: %-5d",p.getName(), p.getPooDamage(), p.getPooStaminaModifier());
			System.out.println(arsenalString);
		}
		System.out.println();
	}
	
	private static void printCommandList() {
		System.out.println("Commands:\n" +
				"attack (Monkey Name) (Poo Name): Attack an enemy monkey with poo from your arsenal.\n" + 
				"moveto (Location): Move to another area in the zoo.\n" +
				"move options: See a list of areas you can currently move into.\n" +
				"pickup (Item name): Pick up an item and add it to your inventory\n"+
				"use (Item name): Use an item from your inventory.\n" +
				"arsenal: Get a list of the types of poo in your arsenal.\n" +
				"inventory: View the items in your inventory.\n"+
				"map: View the map.\n"+
				"details: List the details of each action.\n"+
				"state: View the state of your monkey.\n");	
	}
	
	private static void printError() {
		System.out.println("Sorry, didn't catch that. Check your spelling and try again");
	}
}
