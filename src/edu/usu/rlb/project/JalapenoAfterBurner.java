package edu.usu.rlb.project;

public class JalapenoAfterBurner extends Poo{
	private int healthDeduction = 2;
	private int staminaDeduction = 4;
	private String name ="JalapenoAfterburner";
	
	public JalapenoAfterBurner() {}
	
	public void getChanges(Monkey m){
		m.setHealth(m.getHealth() - healthDeduction);
		m.setStamina(m.getStamina()- staminaDeduction);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void undoChanges(Monkey m) {
		m.setHealth(m.getHealth() + healthDeduction);
		m.setStamina(m.getStamina() + staminaDeduction);
		
	}
	@Override
	public int getPooDamage() {
		return healthDeduction;
	}

	@Override
	public int getPooStaminaModifier() {
		return staminaDeduction;
	}
	
}
