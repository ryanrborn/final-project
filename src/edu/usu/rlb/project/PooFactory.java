package edu.usu.rlb.project;

import java.util.Random;

public class PooFactory {
	public static Poo makeAPoo(){
		Poo product = null;
		Random rand = new Random(); 
		int decider = rand.nextInt(74);
		if(decider == 1) {
			product = new JustAFart();
		}
		else if(decider < 15){
			product = new PelletPoo();
		}
		
		else if(decider < 30) {
			product = new SolidLoaf();
		}
		
		else if(decider < 40) {
			product = new MonkeyMissile();
		}
		
		else if(decider < 55) {
			product = new TurkeySquirts();
		}
		
		else if(decider < 65) {
			product = new JalapenoAfterBurner();
		}
		else if(decider < 70) {
			product = new BigOlTexan();
		}
		else {
			product = new ExplodingButtMud();
		}
			
		return product;
	}
}
