package edu.usu.rlb.project;

public abstract class UseItem implements Command{
	private Item theMagicThingy = null;
	private Monkey luckyFella = null;
	private int time;
	
	public UseItem(Item thing, Monkey dude, int time) {
		this.theMagicThingy = thing;
		this.luckyFella = dude;
		this.time = theMagicThingy.time;
	}
	
	public int getTime(){
		return time;
	}
	
	public void setTime(int time) {
		this.time = time;
	}
	//the time is how long it takes to execute a command
	public void reduceTime(int monkeyStamina) {
		time -= monkeyStamina;
	}
	public String getPooName(){
		return theMagicThingy.getName();
	}

	public String getRecieverName() {
		return luckyFella.getName();
	}
	@Override
	public void execute(){
		this.theMagicThingy.getChanges(luckyFella);
	}
}
