package edu.usu.rlb.project;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Location {
	private String name;
	private List<Location> neighbors = new ArrayList<Location>();
	private Map<String, Item> items = new HashMap<String, Item>();
	private List<Monkey> monkeys = new ArrayList<Monkey>();
	
	public boolean hasItem(String itemName){
		if(items.containsKey(itemName.toLowerCase())){
			return true;
		}
		return false;
	}
	
	public Location(String name){
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
	
	public void monkeyEnters(Monkey m){
		System.out.println(m.getName() + " has entered " + this.name);
		monkeys.add(m);
	}
	
	public void monkeyLeaves(Monkey m){
		int index = monkeys.indexOf(m);
		if(index != -1){
//			System.out.println("  " + m.getName() + " has left " + this.name);
			monkeys.remove(index);
		}
	}
	
	public List<Monkey> getAllMonkeys(){
		return monkeys;
	}
	
	public List<Monkey> getLiveMonkeys(){
		List<Monkey> list = new ArrayList<Monkey>();
		for(Monkey mon : monkeys){
			if(mon.isAlive){
				list.add(mon);
			}
		}
		return list;
	}
	
	public List<Location> getNeighboringLocations(){
		return neighbors;
	}
	
	public int getNumNeighbors(){
		return neighbors.size();
	}
	
	public List<Monkey> getMonkeysInRange(){
		List<Monkey> enemies = new ArrayList<Monkey>();
		for(Location loc : neighbors){
			enemies.addAll(loc.getLiveMonkeys());
		}
		enemies.addAll(getLiveMonkeys());
		return enemies;
	}
	
	public void addNeighbor(Location loc){
		if(neighbors.indexOf(loc) == -1 && loc != this){
			neighbors.add(loc);
		}
	}
	
	public List<String> getItems(){
		List<String> keys = new ArrayList<String>();
		for(Map.Entry<String, Item> entry : items.entrySet()){
			keys.add(entry.getKey());
		}
		return keys;
	}
	
	public Item getItem(String name) throws Exception{
		// find the item to remove
		if(items.containsKey(name)){
			Item item = items.get(name);
			items.remove(name);
			return item;
		}
		throw new Exception("Item ("+name+") not found!");
	}
	
	public void addItem(Item i){
		items.put(i.getName(), i);
		System.out.println("A " + i.getName() + " has been added to the " + this.getName());
	}
	
	public String toString(){
		return name;
	}
}
