package edu.usu.rlb.project;

import java.util.Random;

public class ExplodingButtMud extends Poo{
	
	private int healthDeduction;
	private int staminaDeduction;
	private String name = "ExplodingButtMud";
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ExplodingButtMud(){
		Random rand = new Random();
		this.healthDeduction = rand.nextInt(10) + 1;
		this.staminaDeduction = rand.nextInt(5) + 1;
	}

	@Override
	public void getChanges(Monkey m) {
		m.setHealth(m.getHealth() - healthDeduction);
		m.setStamina(m.getStamina() - staminaDeduction);
	}

	@Override
	public void undoChanges(Monkey m) {
		m.setHealth(m.getHealth() + healthDeduction);
		m.setStamina(m.getStamina() + staminaDeduction);
	}
	@Override
	public int getPooDamage() {
		return healthDeduction;
	}

	@Override
	public int getPooStaminaModifier() {
		return staminaDeduction;
	}

}
