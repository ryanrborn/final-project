package edu.usu.rlb.project;

import java.util.List;
import java.util.Random;

public class MonkeyAggressive extends Monkey{

	public MonkeyAggressive() {
		super("Aggie");
		this.health = 25;
		this.stamina = 15;
	}
	
	public void doTurn(){
		// first we get all monkey's in range
		List<Monkey> monkeys = location.getMonkeysInRange();
		// we need to remove ourselves from this list
		monkeys.remove(monkeys.indexOf(this));
		
		if(monkeys.size() == 0){
			// move
			List<Location> locations = location.getNeighboringLocations();
			int rand = new Random().nextInt(locations.size());
			Location next = locations.get(rand);
			next.monkeyEnters(this);
			location.monkeyLeaves(this);
			location = next;
		}else if(getInvoker().getAttackListSize() == 0){
			// now we find the weakest one
			Monkey weakest = null;
			for(Monkey mon : monkeys){
				if(weakest == null){
					weakest = mon;
				}else if(weakest.getHealth() > mon.getHealth()){
					weakest = mon;
				}
			}
			
			Poo biggest = null;
			for(Poo p : getArsenal()){
				if(biggest == null){
					biggest = p;
				}else if(biggest.getPooDamage() < p.getPooDamage()){
					biggest = p;
				}
			}
			removeFromArsenal(biggest);
			
			addAttack(new FlingPoo(biggest, weakest, 10));
			doAttack();
		}else{
			doAttack();
		}
		while(getArsenal().size() < getArsenalMaxSize()){
			addToArsenal(PooFactory.makeAPoo());
		}
	}

}