package edu.usu.rlb.project;

public class Bucket extends Item{
	private String name = "bucket";
	
	@Override
	public void getChanges(Monkey luckyFella) {
		luckyFella.setArsenalMaxSize(luckyFella.getArsenalMaxSize() + 2);
		System.out.println("Nothing to see here. Just using this bucket has permanently increased the amount of Poo you can carry");
		luckyFella.removeInventoryItem(this);
	}

	@Override
	public String getName() {
		return name;
	}
	
}
