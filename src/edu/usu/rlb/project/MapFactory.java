package edu.usu.rlb.project;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MapFactory {
	public static String[] names = 
		{"MonkeyTree",		"HippoPool",	"BearCave",
		"ZebraCorral",		"Courtyard",	"ElephantEnclosure",
		"ZookeepersOffice",	"TigerDen",		"Aviary"};
	
	public static List<Location> generateMap(List<Monkey> monkeys){
		System.out.println("Generating Map...");
		// okay, same map every time, no random generation
		List<Location> locations = new ArrayList<Location>();
		for(String name : MapFactory.names){
			locations.add(new Location(name));
		}
		// set neighborings
		locations.get(0).addNeighbor(locations.get(1));
		locations.get(0).addNeighbor(locations.get(3));
		locations.get(1).addNeighbor(locations.get(0));
		locations.get(1).addNeighbor(locations.get(2));
		locations.get(1).addNeighbor(locations.get(4));
		locations.get(2).addNeighbor(locations.get(1));
		locations.get(2).addNeighbor(locations.get(5));
		locations.get(3).addNeighbor(locations.get(0));
		locations.get(3).addNeighbor(locations.get(4));
		locations.get(3).addNeighbor(locations.get(6));
		locations.get(4).addNeighbor(locations.get(1));
		locations.get(4).addNeighbor(locations.get(3));
		locations.get(4).addNeighbor(locations.get(5));
		locations.get(4).addNeighbor(locations.get(7));
		locations.get(5).addNeighbor(locations.get(2));
		locations.get(5).addNeighbor(locations.get(4));
		locations.get(5).addNeighbor(locations.get(8));
		locations.get(6).addNeighbor(locations.get(3));
		locations.get(6).addNeighbor(locations.get(7));
		locations.get(7).addNeighbor(locations.get(4));
		locations.get(7).addNeighbor(locations.get(6));
		locations.get(7).addNeighbor(locations.get(8));
		locations.get(8).addNeighbor(locations.get(5));
		locations.get(8).addNeighbor(locations.get(7));
		

		Shovel shovel = new Shovel();
		Towel towel = new Towel();
		Towel towel2 = new Towel();
		Bucket bucket =  new Bucket();
		
		System.out.println("Seeding map with items:");
		locations.get(6).addItem(shovel);
		MapFactory.pause();
		locations.get(5).addItem(bucket);
		MapFactory.pause();
		locations.get(1).addItem(towel);
		MapFactory.pause();
		locations.get(0).addItem(towel2);
		MapFactory.pause();
		Random rand = new Random();
		// use two loops for readability in the console
		for(int i=0; i<monkeys.size(); i++){
			Banana b =  new Banana();
			locations.get(rand.nextInt(locations.size())).addItem(b);	
			MapFactory.pause();
		}
		System.out.println("\nPlacing monkeys randomly throughout map:");
		for(Monkey m: monkeys) {
			int locIndex = rand.nextInt(locations.size());
			locations.get(locIndex).monkeyEnters(m);
			m.location = locations.get(locIndex);
			MapFactory.pause();
		}
		return locations;
	}
	
	public static void pause(){
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
