package edu.usu.rlb.project;

import java.util.List;
import java.util.Random;

public class MonkeySurvivor extends Monkey{

	public MonkeySurvivor() {
		super("SirVive");
		this.health = 15;
		this.stamina = 25;
	}

	public void doTurn(){
		// first we get all monkey's in range
		List<Monkey> monkeys = location.getMonkeysInRange();
		// we need to remove ourselves from this list
		monkeys.remove(monkeys.indexOf(this));

		if(monkeys.size() == 0){
			// move
			List<Location> locations = location.getNeighboringLocations();
			int rand = new Random().nextInt(locations.size());
			Location next = locations.get(rand);
			next.monkeyEnters(this);
			location.monkeyLeaves(this);
			location = next;
		}else if(invoker.attackList.size() == 0){
			// now we find the strongest one
			Monkey strongest = null;
			for(Monkey mon : monkeys){
				if(strongest == null){
					strongest = mon;
				}else if(strongest.getHealth() < mon.getHealth()){
					strongest = mon;
				}
			}

			Poo biggest = null;
			for(Poo p : getArsenal()){
				if(biggest == null){
					biggest = p;
				}else if(biggest.getPooDamage() < p.getPooDamage()){
					biggest = p;
				}
			}
			removeFromArsenal(biggest);

			addAttack(new FlingPoo(biggest, strongest, 10));
			doAttack();
		}else{
			doAttack();
		}
		while(getArsenal().size() < getArsenalMaxSize()){
			addToArsenal(PooFactory.makeAPoo());
		}
	}
}
